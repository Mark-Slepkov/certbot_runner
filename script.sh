#!/usr/bin/bash
echo $DOMAIN
echo $EMAIL
if ls /etc/letsencrypt/live/$DOMAIN;
then
    certbot --standalone --preferred-challenges http renew
else
    certbot certonly --authenticator standalone -d $DOMAIN -n --agree-tos --email $EMAIL
fi