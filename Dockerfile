FROM ubuntu:16.04
MAINTAINER Mark Slepkov self@mark-slepkov.ru
ARG DOMAIN
ENV DOMAIN=$DOMAIN
ARG EMAIL
ENV EMAIL=$EMAIL
EXPOSE 80
RUN apt-get update -qy && apt-get install -y software-properties-common && add-apt-repository ppa:certbot/certbot && apt-get update && apt-get install -y python-certbot-nginx
WORKDIR /app
COPY runner.sh ./runner.sh
COPY script.sh ./script.sh
COPY run_once.sh ./run_once.sh
CMD bash ./runner.sh
